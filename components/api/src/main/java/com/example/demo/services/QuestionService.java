package com.example.demo.services;

import com.example.demo.dto.OptionInfo;
import com.example.demo.dto.QuestionInfo;
import com.example.demo.model.Option;
import com.example.demo.model.Question;
import com.example.demo.model.Survey;
import com.example.demo.repository.QuestionRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service("QuestionService")
public class QuestionService implements QuestionRepository {
    Logger logger = LoggerFactory.getLogger(QuestionService.class);

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private SurveyService surveyRepository;

    @Override
    public Question addNewQuestion(String name, boolean multipleVotes, long surveyId) {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            Question question = new Question();
            question.name = name;
            question.multipleVotes = multipleVotes;
            question.surveyId = surveyId;
            transaction = session.beginTransaction();
            session.save(question);
            transaction.commit();
            return question;
        } catch (Exception e) {
            logger.debug(e.getMessage(), e);
            if (transaction != null) {
                transaction.rollback();
            }
        }
        return null;
    }

    @Override
    public int getQuestionNumberInSurvey(long surveyId) {
        int size = 0;
        try (Session session = sessionFactory.openSession()) {
            String query = "FROM Question where survey_id = " + surveyId;
            var list = session.createQuery(query, Question.class).list();
            size = list.size();
        }
        return size;
    }

    @Override
    public List<QuestionInfo> getQuestionsFromSurveyByTitle(String title){
        List<QuestionInfo> resultList = new ArrayList<>();
        List<Option> listOfOptions = new ArrayList<>();
        try (Session session = sessionFactory.openSession()) {
            var survey = surveyRepository.findSurveyByTitle(title);
            if(survey != null){
                String query = "FROM Question where survey_id = " + survey.id;
                var listOfQuestions = session.createQuery(query, Question.class).list();
                for(var el : listOfQuestions){
                    var res = new QuestionInfo();
                    res.id = el.id;
                    res.multipleVotes = el.multipleVotes;
                    res.question = el.name;
                    res.options = new ArrayList<>();

                    query = "FROM Option where question_id = " + el.id;
                    listOfOptions = session.createQuery(query, Option.class).list();
                    res.options = listOfOptions;
//                    for(var option: listOfOptions){
//                        var optionInfo = new OptionInfo();
//                        optionInfo.id = option.id;
//                        optionInfo.answer = option.name;
//                        res.options.add(optionInfo);
//                    }
                    resultList.add(res);
                }
            }

        }
        return resultList;
    }


    @Override
    public Question findQuestionById(long id) {
        try (Session session = sessionFactory.openSession()) {
            String query = "FROM Question WHERE id =" + id;
            var questionOptional = session.createQuery(query, Question.class).uniqueResultOptional();
            return questionOptional.orElse(null);
        } catch (Exception e) {
            logger.debug(e.getMessage(), e);
        }
        return null;
    }
}
