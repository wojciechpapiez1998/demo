package com.example.demo.model;

import javax.persistence.*;

@Entity
@Table(name = "answer")
public class Answer{

    @Id
    @GeneratedValue
    public long id;
    public long questionid;
    public long optionId;
    public long voteId;

}
