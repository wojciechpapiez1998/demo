package com.example.demo.repository;

import com.example.demo.dto.SurveyWithNumberOfQuestion;
import com.example.demo.model.Question;
import com.example.demo.model.Survey;
import com.example.demo.model.User;

import java.util.List;

public interface SurveyRepository {
    Survey addNewSurvey(String title) throws Exception;
    Boolean checkIfSurveyTitleExists(String login);
    Survey getLastAddedSurvey();
    Survey updateSurveyTitle(Survey survey, String title);
    Survey findSurveyById(long id);
    Survey findSurveyByTitle(String title);
    List<SurveyWithNumberOfQuestion> getAllSurveysWithQuestionNumberInfo();
    boolean deleteSurvey(String title);
}
