export default class SurveyTitleWithQuestionNumber {
    constructor() {
        this.id = null;
        this.title = null;
        this.questionNumber = null;
    }

}
