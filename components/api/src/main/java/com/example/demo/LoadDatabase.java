package com.example.demo;

import com.example.demo.model.Role;
import com.example.demo.model.User;
import com.example.demo.repository.RoleRepository;
import com.example.demo.repository.UserRepository;
import com.example.demo.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LoadDatabase {

    private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);

    @Bean
    CommandLineRunner initDatabase(UserService userRepository, RoleRepository roleRepository) {
        System.out.println(org.hibernate.Version.getVersionString());
        System.out.println(org.hibernate.Version.getVersionString());
        System.out.println(org.hibernate.Version.getVersionString());
        System.out.println(org.hibernate.Version.getVersionString());
        return args -> {
            if (!roleRepository.checkIfRoleExists("Owner")) {
                log.info("Preloading " + roleRepository.createRole("Owner"));
            }
            if (!roleRepository.checkIfRoleExists("Admin")) {
                log.info("Preloading " + roleRepository.createRole("Admin"));

            }
            if (!roleRepository.checkIfRoleExists("User")) {
                log.info("Preloading " + roleRepository.createRole("User"));
            }
            if (!userRepository.checkIfUserEmailExists("owner@domain")) {
                if (!userRepository.checkIfUserLoginExists("owner")) {
                    Role role = roleRepository.findRoleByName("Owner");
                    log.info("Preloading " + userRepository.registerAccount(new User("owner@domain", "owner", "owner", role.id)));
                }
            }

        };
    }
}