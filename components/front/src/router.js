import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
    mode: "history",
    routes: [
        {
            path: "/",
            name: "app",
            component: () => import("./components/Dashboard")
        },
        {
            path: "/sign-in",
            name: "sign-in",
            component: () => import("./components/SignIn")
        },
        {
            path: "/users-management",
            name: "UsersManagement",
            component: () => import("./components/UsersManagement")
        },
        {
            path: "/my-account",
            name: "MyAccount",
            component: () => import("./components/MyAccount")
        },

        {
            path: "/poll-service",
            name: "PollCreatorService",
            component: () => import("./components/PollCreatorService")
        },
        {
            path: "/survey-lists",
            name: "SurveyLists",
            component: () => import("./components/SurveyLists")
        },
        {
            path: "/poll-answer-service",
            name: "PollAnswerService",
            component: () => import("./components/PollAnswerService")
        },
        {
            path: "/token-encoder",
            name: "TokenEncoder",
            component: () => import("./components/TokenEncoder")
        },
    ]
});