export default class User{
    constructor() {
        this.login = null;
        this.email = null;
        this.password = null;
        this.password2 = null;
    }


    /**
     * @param value {string}
     * @returns {User}
     */
    setLogin(value){
        this.login = value;
        return this;
    }
    /**
     * @returns {string}
     */
    getLogin(){
        return this.login;
    }


    /**
     * @param value {string}
     * @returns {User}
     */
    setEmail(value){
        this.email = value;
        return this;
    }
    /**
     * @returns {string}
     */
    getEmail(){
        return this.email;
    }


    /**
     * @param value {string}
     * @returns {User}
     */
    setPassword(value){
        this.password = value;
        return this;
    }
    /**
     * @returns {string}
     */
    getPassword(){
        return this.password;
    }
}
