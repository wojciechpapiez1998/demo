package com.example.demo.services;

import com.example.demo.model.Role;
import com.example.demo.model.User;
import com.example.demo.repository.RoleRepository;
import com.example.demo.repository.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.util.List;


@Service("UserService")
public class UserService implements UserRepository {
    Logger logger = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private RoleRepository roleRepository;


    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }

    public Session getSession() {
        if (!sessionFactory.isOpen()) {
            return sessionFactory.openSession();
        }
        return sessionFactory.getCurrentSession();
    }

    @Override
    public User registerAccount(User user) {
        var isUserExists = checkIfUserEmailExists(user.email);
        if (isUserExists) {
            return null;
        }
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            var passwordEncoder = encoder();
            User appUser = new User();
            appUser.password = passwordEncoder.encode(user.password);
            appUser.email = user.email;
            if (user.roleId == null) {
                Role role = roleRepository.findRoleByName("User");
                appUser.roleId = role.id;
            } else {
                appUser.roleId = user.roleId;
            }
            var ownerRole = roleRepository.findRoleByName("owner");
            Long ownerRoleId = ownerRole.id;
            if (ownerRoleId.equals(appUser.roleId)) {
                appUser.login = user.login;
            }

            transaction = session.beginTransaction();

            session.save(appUser);
            transaction.commit();
            return appUser;
        } catch (Exception e) {
            logger.debug(e.getMessage(), e);
            if (transaction != null) {
                transaction.rollback();
            }
        }
        return null;
    }

    @Override
    public User signIn(User user) {
        User applicationUser = null;
        if (user.login != null) {
            applicationUser = findUserByLogin(user.login);
        } else {
            applicationUser = findUserByEmail(user.email);
        }

        if (applicationUser != null) {
            var passwordEncoder = encoder();
            var correctHashedPassword = passwordEncoder.matches(user.password, applicationUser.password);
            var correctPassword = user.password.equals(applicationUser.password);
            if (correctPassword || correctHashedPassword) {
                return applicationUser;
            }
        }
        return null;
    }

    @Override
    public User findUserByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            String query = "from User where email = '" + email.trim() + "'";
            var user = session.createQuery(query, User.class).uniqueResultOptional();
            return user.orElse(null);
        } catch (Exception e) {
            logger.debug(e.getMessage(), e);
            return null;
        }
    }

    @Override
    public User findUserByLogin(String login) {
        try (Session session = sessionFactory.openSession()) {
            String query = "from User where login = '" + login.trim() + "'";
            return getUser(session, query);
        } catch (Exception e) {
            logger.debug(e.getMessage(), e);
            return null;
        }
    }

    @Override
    public User findUserById(long id) {
        try (Session session = sessionFactory.openSession()) {
            String query = "FROM User WHERE id =" + id;
            return getUser(session, query);
        } catch (Exception e) {
            logger.debug(e.getMessage(), e);
            return null;
        }

    }

    private User getUser(Session session, String query) {
        var userOptional = session.createQuery(query, User.class).uniqueResultOptional();
        if (userOptional.isPresent()) {
            var user = userOptional.get();
            if (user.login == null) {
                user.login = "";
            }
            return user;
        }
        return null;
    }

    @Override
    public boolean checkIfUserLoginExists(String login) {
        Session session = sessionFactory.openSession();
        String query = "from User where login = '" + login.trim() + "'";
        var result = session.createQuery(query, User.class).uniqueResultOptional();
        session.close();
        return result.isPresent();
    }

    @Override
    public boolean checkIfUserEmailExists(String email) {
        Session session = sessionFactory.openSession();
        String query = "from User where email = '" + email.trim() + "'";
        var result = session.createQuery(query, User.class).uniqueResultOptional();
        session.close();
        return result.isPresent();
    }

    @Override
    public boolean validateEmail(String email) {
        return checkIfUserEmailExists(email);
    }

    @Override
    public boolean validateDomainEmail(String email) {
//        return true;
        try {
            if (!email.isEmpty()) {
                InternetAddress emailAddress = new InternetAddress(email);
                emailAddress.validate();
            }
        } catch (AddressException ex) {
            return false;
        }
        return true;
    }

    @Override
    public User updateUser(long userId, User user) {
        Transaction transaction = null;
        User appUser = null;
        try (Session session = sessionFactory.openSession()) {
            appUser = findUserById(userId);
            if (!appUser.login.equals(user.login)) {
                var isUserExists = checkIfUserLoginExists(user.login);
                if (isUserExists) {
                    return null;
                }
            }
            if (!appUser.email.equals(user.email)) {
                var isUserExists = checkIfUserEmailExists(user.email);
                if (isUserExists) {
                    return null;
                }
            }
            appUser.login = user.login;
            appUser.email = user.email;
            if (!user.password.isEmpty()) {
                var passwordEncoder = encoder();
                appUser.password = passwordEncoder.encode(user.password);
            }
            transaction = session.beginTransaction();
            session.update(appUser);
            session.save(appUser);
            transaction.commit();

        } catch (Exception e) {
            logger.debug(e.getMessage(), e);
            if (transaction != null) {
                transaction.rollback();
            }
        }
        return appUser;
    }

    @Override
    public List<User> getAllUsers() {
        try (Session session = sessionFactory.openSession()) {
            String query = "FROM User where email != 'owner@domain'";
            return session.createQuery(query, User.class).list();
        } catch (Exception e) {
            logger.debug(e.getMessage(), e);
            return null;
        }
    }

    @Override
    public boolean deleteUser(long id) {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            String query = "FROM User where id = " + id;
            var userOptional = session.createQuery(query, User.class).uniqueResultOptional();
            if (userOptional.isPresent()) {
                var user = userOptional.get();
                if (user.login == null) {
                    user.login = "";
                }
                transaction = session.beginTransaction();
                session.delete(user);
                transaction.commit();
                return true;
            }
        } catch (Exception e) {
            logger.debug(e.getMessage(), e);
            if (transaction != null) {
                transaction.rollback();
            }
        }
        return false;
    }


    @Override
    public User addAdminRole(long id) {
        Transaction transaction = null;
        User user = null;
        try (Session session = sessionFactory.openSession()) {
            user = findUserById(id);
            user.roleId = roleRepository.findRoleByName("Admin").id;
            if(user.login.isEmpty()){
                user.login = null;
            }
            transaction = session.beginTransaction();
            session.update(user);
            session.save(user);
            transaction.commit();

        } catch (Exception e) {
            logger.debug(e.getMessage(), e);
            if (transaction != null) {
                transaction.rollback();
            }
        }
        return user;
    }

    @Override
    public User removeAdminRole(long id) {
        Transaction transaction = null;
        User user = null;
        try (Session session = sessionFactory.openSession()) {
            user = findUserById(id);
            user.roleId = roleRepository.findRoleByName("User").id;
            if(user.login.isEmpty()){
                user.login = null;
            }
            transaction = session.beginTransaction();
            session.update(user);
            session.save(user);
            transaction.commit();

        } catch (Exception e) {
            logger.debug(e.getMessage(), e);
            if (transaction != null) {
                transaction.rollback();
            }
        }
        return user;
    }

}
