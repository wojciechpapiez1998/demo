import http from "../http-common";
import {bus} from "@/main";

class PollService {

    async savePollUrl(surveyInfo) {
        let res = (await http.post("/post-poll-url", surveyInfo)).data;
        if (res) {
            throw new this.surveyTitleException(res);
        }
        else{
            await bus.$emit('new-poll');
        }
    }

    async getAllSurveys() {
        return (await http.get("/list-surveys")).data;
    }

    async getQuestionsFromSurvey(data) {
        return  (await http.post("/list-questions", data)).data;
    }

    async deleteSurvey(data) {
        return (await http.post("/delete/survey", data)).data;
    }

    async vote(data) {
        return (await http.post("/vote", data)).data;
    }

    async getSurveyByToken(data) {
        return (await http.get(`/token/survey/${data}`)).data;
    }

    async getAnswersByToken(data) {
        return (await http.get(`/token/answers/${data}`)).data;
    }

    surveyTitleException(message) {
        this.message = message;
        this.name = 'surveyTitleException';
    }
}

export default new PollService();