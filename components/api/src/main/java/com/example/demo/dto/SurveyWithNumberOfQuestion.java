package com.example.demo.dto;

import com.example.demo.model.Survey;

public class SurveyWithNumberOfQuestion {
    public int id;
    public Survey survey;
    public int numberOfQuestion;

    public SurveyWithNumberOfQuestion(int id, Survey survey, int numberOfQuestion) {
        this.id = id;
        this.survey = survey;
        this.numberOfQuestion = numberOfQuestion;
    }
}
