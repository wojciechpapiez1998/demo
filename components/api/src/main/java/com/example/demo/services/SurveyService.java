package com.example.demo.services;


import com.example.demo.dto.SurveyWithNumberOfQuestion;
import com.example.demo.model.Role;
import com.example.demo.model.Survey;
import com.example.demo.model.User;
import com.example.demo.repository.SurveyRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service("SurveyService")
public class SurveyService implements SurveyRepository {
    Logger logger = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private QuestionService questionRepository;

    @Override
    public Survey addNewSurvey(String title) {
        var isTitleExists = checkIfSurveyTitleExists(title);
        if (isTitleExists) {
            return null;
        }
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            Survey survey = new Survey();
            survey.title = title;
            transaction = session.beginTransaction();
            session.save(survey);
            transaction.commit();
            return survey;
        } catch (Exception e) {
            logger.debug(e.getMessage(), e);
            if (transaction != null) {
                transaction.rollback();
            }
        }
        return null;
    }

    @Override
    public Boolean checkIfSurveyTitleExists(String title) {
        try (Session session = sessionFactory.openSession()) {
            String query = "from Survey where title = '" + title.trim() + "'";
            var result = session.createQuery(query, Survey.class).uniqueResultOptional();
            return result.isPresent();
        } catch (Exception e) {
            logger.debug(e.getMessage(), e);
            return null;
        }
    }

    @Override
    public Survey getLastAddedSurvey() {
        try (Session session = sessionFactory.openSession()) {
            String query = "FROM Survey where id = (select max(id) from Survey)";
            var survey = session.createQuery(query, Survey.class).uniqueResultOptional();
            return survey.orElse(null);
        } catch (Exception e) {
            logger.debug(e.getMessage(), e);
            return null;
        }
    }

    @Override
    public Survey updateSurveyTitle(Survey survey, String title) {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            survey.title = title;

            transaction = session.beginTransaction();
            session.update(survey);
            session.save(survey);
            transaction.commit();

        } catch (Exception e) {
            logger.debug(e.getMessage(), e);
            if (transaction != null) {
                transaction.rollback();
            }
        }
        return survey;
    }

    @Override
    public Survey findSurveyById(long id) {
        try (Session session = sessionFactory.openSession()) {
            String query = "FROM Survey WHERE id =" + id;
            var surveyOptional = session.createQuery(query, Survey.class).uniqueResultOptional();
            return surveyOptional.orElse(null);
        } catch (Exception e) {
            logger.debug(e.getMessage(), e);
        }
        return null;
    }

    @Override
    public Survey findSurveyByTitle(String title) {
        try (Session session = sessionFactory.openSession()) {
            String query = "FROM Survey where title = '" + title.trim() + "'";
            var surveyOptional = session.createQuery(query, Survey.class).uniqueResultOptional();
            return surveyOptional.orElse(null);
        } catch (Exception e) {
            logger.debug(e.getMessage(), e);
        }
        return null;
    }

    @Override
    public List<SurveyWithNumberOfQuestion> getAllSurveysWithQuestionNumberInfo() {
        List<SurveyWithNumberOfQuestion> result = new ArrayList<>();
        try (Session session = sessionFactory.openSession()) {
            String query = "FROM Survey where deleted = false";
            var list = session.createQuery(query, Survey.class).list();
            for (int i = 0; i < list.size(); i++) {
                var id = list.get(i).id;
                var numberOfQuestions = questionRepository.getQuestionNumberInSurvey(id);
                result.add(new SurveyWithNumberOfQuestion(i+1, list.get(i), numberOfQuestions));
            }
        } catch (Exception e) {
            logger.debug(e.getMessage(), e);
            return null;
        }
        return result;
    }

    @Override
    public boolean deleteSurvey(String title) {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            String query = "FROM Survey where title = '" + title.trim() + "'";
            var surveyOptional = session.createQuery(query, Survey.class).uniqueResultOptional();
            if (surveyOptional.isPresent()) {
                var survey = surveyOptional.get();
                survey.deleted = true;

                transaction = session.beginTransaction();
                session.save(survey);
                transaction.commit();
                return true;
            }
        } catch (Exception e) {
            logger.debug(e.getMessage(), e);
            if (transaction != null) {
                transaction.rollback();
            }
        }
        return false;
    }

}
