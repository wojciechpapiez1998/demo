package com.example.demo.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "vote")
public class Vote{

    @Id
    @GeneratedValue
    public Long id;
    public String hashedIdentifier;
    public Date date;
    public long surveyId;

}
