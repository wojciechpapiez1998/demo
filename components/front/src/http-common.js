import axios from "axios";
// plik konfiguracyjny axiosa
export default axios.create({
    baseURL: "http://localhost:8080/api",
    headers: {
        "Content-type": "application/json"
    }
});