package com.example.demo.controller;

import com.example.demo.model.Role;
import com.example.demo.model.User;
import com.example.demo.repository.RoleRepository;
import com.example.demo.services.RoleService;
import com.example.demo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.*;


@CrossOrigin(origins = "http://localhost:8081")
@RestController
@RequestMapping("/api")
public class UserController {

    @Autowired
    private UserService userRepository;

    @Autowired
    private RoleService roleRepository;


    private final Session session;

    public UserController(){
        session = Session.getInstance();
    }


    @PostMapping("/user/signUp")
    public User signUp(@RequestBody User user) {
        return userRepository.registerAccount(user);
    }

    @GetMapping("/user/sessionInfo")
    public User getUserSessionInfo() {
        return session.getUserSession();
    }

    @PostMapping("/user/signIn")
    public User signIn(@RequestBody User user){
        var appUser = userRepository.signIn(user);
        if(appUser != null){
            session.setUserSession(appUser);
        }
        return appUser;
    }

    @GetMapping("/user/signOut")
    public void signOut(){
        session.setUserSession(null);
    }

    @PostMapping("/user/validateLogin")
    public boolean isLoginInUsed(@RequestBody String login){
        return userRepository.checkIfUserEmailExists(login);
    }

    @PostMapping("/user/validateEmailDomain")
    public boolean validateEmailDomain(@RequestBody String email){
        return userRepository.validateDomainEmail(email);
    }

    @PostMapping("/user/validateEmail")
    public boolean validateEmail(@RequestBody String email){
        return userRepository.validateEmail(email);
    }

    @GetMapping("/roles/{id}")
    public Role getUserRole(@PathVariable("id") long userId) {
        return roleRepository.findRoleById(userId);
    }

    @GetMapping("/roles/admin")
    public Role getAdminRole() {
        return roleRepository.findRoleByName("Admin");
    }

    @PutMapping("/user/{id}")
    public User editMyAccount(@PathVariable("id") long userId, @RequestBody User user) {
        var appUser = userRepository.updateUser(userId, user);
        if(appUser != null){
            session.setUserSession(appUser);
        }
        return appUser;
    }

    @GetMapping("/user/getAll")
    public List<User> getAllUsers() {
        return userRepository.getAllUsers();
    }

    @DeleteMapping("/user/del/{id}")
    public boolean deleteUser(@PathVariable("id")  long id) {
        return userRepository.deleteUser(id);
    }

    @PostMapping("/user/addAdmin/{id}")
    public User addAdmin(@PathVariable("id")  long id) {
        return userRepository.addAdminRole(id);
    }

    @PostMapping("/user/delAdmin/{id}")
    public User deleteAdmin(@PathVariable("id")  long id) {
        return userRepository.removeAdminRole(id);
    }

    @GetMapping("/user/check-if/admin/owner/{id}")
    public boolean checkIfUserIsAdminOrOwner(@PathVariable("id") long userId) {
        return roleRepository.checkIfUserIsAdminOrOwner(userId);
    }

    @GetMapping("/user/check-if/owner/{id}")
    public boolean checkIfUserIsOwner(@PathVariable("id") long userId) {
        return roleRepository.checkIfUserIsOwner(userId);
    }

}
