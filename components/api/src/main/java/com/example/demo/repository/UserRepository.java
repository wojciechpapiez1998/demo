package com.example.demo.repository;

import com.example.demo.model.User;

import java.util.List;

public interface UserRepository {
    User registerAccount(User user);
    User signIn(User user);
    User findUserByEmail(String email);
    User findUserByLogin(String login);
    User findUserById(long id);
    User updateUser(long userId, User user);
    boolean checkIfUserLoginExists(String login);
    boolean checkIfUserEmailExists(String email);
    boolean validateDomainEmail(String email);
    boolean validateEmail(String email);
    List<User> getAllUsers();
    boolean deleteUser(long id);
    User addAdminRole(long id);
    User removeAdminRole(long id);
}
