package com.example.demo.repository;

import com.example.demo.model.Role;
import com.example.demo.model.User;

public interface RoleRepository {
    Role createRole(String name);
    Boolean checkIfRoleExists(String name);
    Role findRoleByName(String name);
    Role findRoleById(long id);
    boolean checkIfUserIsAdminOrOwner(long userId);
    boolean checkIfUserIsOwner(long userId);
}
