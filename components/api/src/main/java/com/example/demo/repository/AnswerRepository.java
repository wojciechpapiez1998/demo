package com.example.demo.repository;

import com.example.demo.dto.AnswerWithQuestionNumber;
import com.example.demo.model.Answer;

import java.util.List;

public interface AnswerRepository {
    String addNewAnswer(List<AnswerWithQuestionNumber> answerWithQuestionNumber);
    List<Answer> getAllUserAnswersInVote(long id);
}
