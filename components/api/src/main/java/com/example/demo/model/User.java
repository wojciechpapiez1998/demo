package com.example.demo.model;


import javax.persistence.*;

@Entity
@Table(name = "user")
public class User{

    @Id
    @GeneratedValue
    public Long id;
    @Column(unique=true)
    public String email;
    @Column(unique=true)
    public String login;
    public String password;
    public Long roleId;

    public User(String email, String password, long roleId){
        this.email = email;
        this.password = password;
        this.roleId = roleId;
    }

    public User(String email, String login, String password, long roleId){
        this.email = email;
        this.login = login;
        this.password = password;
        this.roleId = roleId;
    }

    public User(){
    }


}
