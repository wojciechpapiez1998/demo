export let MIN_PASSWORD_LENGTH = 6;
export let MAX_PASSWORD_LENGTH = 254;
export let MIN_LOGIN_LENGTH = 3;
export let MAX_LOGIN_LENGTH = 254;
export let MAX_INPUT_LENGTH = 254;
