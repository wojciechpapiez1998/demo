import PollView from './PollAnswer.vue';
import PollCreator from './PollCreator.vue'

const Poll = {
  PollView,
  PollCreator
}

export default Poll

export {
  Poll,
  PollView,
  PollCreator
}