package com.example.demo.dto;

public class SurveyInfo {
    public String question;
    public String answers;
    public String multipleVotes;
}
