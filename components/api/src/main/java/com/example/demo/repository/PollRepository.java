package com.example.demo.repository;

import com.example.demo.model.Question;
import com.example.demo.model.Survey;

public interface PollRepository {
    Survey addNewSurvey(Question question);
    Survey findSurveyByName(Question question);
}
