package com.example.demo.controller;

import com.example.demo.dto.AnswerWithQuestionNumber;
import com.example.demo.dto.QuestionInfo;
import com.example.demo.dto.SurveyWithNumberOfQuestion;
import com.example.demo.model.Answer;
import com.example.demo.model.Question;
import com.example.demo.services.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@CrossOrigin(origins = "http://localhost:8081")
@RestController
@RequestMapping("/api")
public class PollController {

    @Autowired
    private SurveyService surveyRepository;

    @Autowired
    private QuestionService questionRepository;

    @Autowired
    private OptionService optionRepository;

    @Autowired
    private AnswerService answerRepository;

    @Autowired
    private VoteService voteRepository;

    @PostMapping("/post-poll-url")
    public String addQuestion(@RequestBody String surveyInfo) throws JsonProcessingException {
        var res = new ObjectMapper().readValue(surveyInfo, HashMap.class);
        var pollMap = (HashMap) res.get("poll");
        String title = res.get("title").toString();
        int questionNumber = (int) res.get("questionNumber");
        String questionName = pollMap.get("question").toString();
        Boolean multipleVotes = (Boolean) pollMap.get("multipleVotes");
        var answers = (ArrayList) pollMap.get("answers");

        Question question = null;
        if(questionNumber == 1){
            var survey = surveyRepository.addNewSurvey(title);
            if(survey == null){
                return "Survey title already exists";
            }
            question = questionRepository.addNewQuestion(questionName, multipleVotes, survey.id);
        }
        else{
            var survey = surveyRepository.getLastAddedSurvey();
            if(survey != null){
                if(!title.equals(survey.title)){
                    surveyRepository.updateSurveyTitle(survey, title);
                }
                question = questionRepository.addNewQuestion(questionName, multipleVotes, survey.id);
            }
        }
        if(question != null){
            for (Object answer : answers) {
                var optionNameMap = (HashMap) answer;
                var optionName = optionNameMap.get("answer").toString();
                optionRepository.addNewOption(optionName, question.id);
            }
        }
        return "";
    }

    @GetMapping("/list-surveys")
    public List<SurveyWithNumberOfQuestion> listOfAllActiveSurveys() {
        return surveyRepository.getAllSurveysWithQuestionNumberInfo();
    }

    @PostMapping("/delete/survey")
    public boolean deleteSurvey(@RequestBody String title) {
        return surveyRepository.deleteSurvey(title);
    }

    @PostMapping("/list-questions")
    public List<QuestionInfo> getQuestionsFromSurvey(@RequestBody String title) {
        return questionRepository.getQuestionsFromSurveyByTitle(title);
    }

    @PostMapping("/vote")
    public String vote(@RequestBody List<AnswerWithQuestionNumber> answerWithQuestionNumber) {
        return answerRepository.addNewAnswer(answerWithQuestionNumber);
    }

    @GetMapping("/token/survey/{data}")
    public List<QuestionInfo> getSurveyByToken(@PathVariable("data") String token) {
        return voteRepository.getSurveyByToken(token);
    }

    @GetMapping("/token/answers/{data}")
    public List<Answer> getAnswersByToken(@PathVariable("data") String token) {
        return voteRepository.getAnswersByToken(token);
    }
}
