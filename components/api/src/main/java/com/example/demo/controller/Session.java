package com.example.demo.controller;

import com.example.demo.model.User;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

import javax.inject.Singleton;

public final class Session {

    private static Session session;
    private User user = null;

    private Session() {
    }

    public static Session getInstance() {
        if(session == null) {
            session = new Session();
        }

        return session;
    }

    public void setUserSession(User user){
        this.user = user;
    }

    public User getUserSession(){
        if(this.user != null && this.user.login== null){
            this.user.login = "";
        }
        return this.user;
    }
}