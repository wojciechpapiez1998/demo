package com.example.demo.services;

import com.example.demo.dto.AnswerWithQuestionNumber;
import com.example.demo.model.Answer;
import com.example.demo.model.Vote;
import com.example.demo.repository.AnswerRepository;
import com.example.demo.tokens.VoteHasher;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;


@Service("AnswerService")
public class AnswerService implements AnswerRepository {
    Logger logger = LoggerFactory.getLogger(AnswerService.class);

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private VoteService voteRepository;

    @Autowired
    private AnswerService answerRepository;

    @Autowired
    private OptionService optionRepository;


    @Override
    public String addNewAnswer(List<AnswerWithQuestionNumber> answerWithQuestionNumber) {
        Transaction transaction = null;
        Vote vote = null;
        int actualQuestionNumber = 0;
        int numberOfAllQuestionsInSurvey = 0;
        Answer answer = new Answer();
        try (Session session = sessionFactory.openSession()) {
            for (var el : answerWithQuestionNumber) {
                actualQuestionNumber = el.actualQuestionNumber;
                numberOfAllQuestionsInSurvey = el.numberOfQuestions;
                answer = new Answer();
                var option = optionRepository.findOptionById(el.option.id);
                option.votes += 1;

                transaction = session.beginTransaction();
                session.update(option);
                session.save(option);
                transaction.commit();

                answer.optionId = el.option.id;
                answer.questionid = el.option.questionId;

                if (actualQuestionNumber == 1 && vote == null) {
                    vote = voteRepository.addNewVote(answer.questionid);
                } else {
                    vote = voteRepository.getLastAddedVote();
                }
                answer.voteId = vote.id;

                transaction = session.beginTransaction();
                session.save(answer);
                transaction.commit();
            }

            if (actualQuestionNumber == 1 && vote == null) {
                vote = voteRepository.addNewVote(answer.questionid);
            } else {
                vote = voteRepository.getLastAddedVote();
            }

            if (actualQuestionNumber == numberOfAllQuestionsInSurvey) {
                VoteHasher voteHasher = new VoteHasher();
                var answers = answerRepository.getAllUserAnswersInVote(vote.id);
                vote.hashedIdentifier = voteHasher.hashVote(vote, answers);
                transaction = session.beginTransaction();
                session.update(vote);
                session.save(vote);
                transaction.commit();
                return vote.hashedIdentifier;
            }


        } catch (Exception e) {
            logger.debug(e.getMessage(), e);
            if (transaction != null) {
                transaction.rollback();
            }
        }

        return null;
    }

    @Override
    public List<Answer> getAllUserAnswersInVote(long voteId) {
        try (Session session = sessionFactory.openSession()) {
            String query = "FROM Answer where vote_id = " + voteId;
            return session.createQuery(query, Answer.class).list();
        }
    }
}
