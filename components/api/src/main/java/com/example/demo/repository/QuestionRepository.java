package com.example.demo.repository;

import com.example.demo.dto.QuestionInfo;
import com.example.demo.model.Question;
import com.example.demo.model.Survey;

import java.util.List;

public interface QuestionRepository {
    Question addNewQuestion(String name, boolean multipleVotes, long surveyId);
    int getQuestionNumberInSurvey(long surveyId);
    List<QuestionInfo> getQuestionsFromSurveyByTitle(String title);
    Question findQuestionById(long id);
}
