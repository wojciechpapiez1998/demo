package com.example.demo.model;


import javax.persistence.*;

@Entity
@Table(name = "option")
public class Option{

    @Id
    @GeneratedValue
    public Long id;
    public String name;
    public Long questionId;
    public long votes = 0;
}
