package com.example.demo.dto;


import com.example.demo.model.Option;

import java.util.HashMap;
import java.util.List;

public class QuestionInfo {
    public long id;
    public boolean multipleVotes;
    public String question;
    public List<Option> options;
}
