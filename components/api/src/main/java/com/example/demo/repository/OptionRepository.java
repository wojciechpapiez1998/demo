package com.example.demo.repository;

import com.example.demo.model.Option;
import com.example.demo.model.Question;
import com.example.demo.model.Survey;

public interface OptionRepository {
    Option addNewOption(String name, long questionId);
    Option findOptionById(long id);
}
