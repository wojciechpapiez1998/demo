package com.example.demo.dto;


import com.example.demo.model.Option;

public class AnswerWithQuestionNumber {
    public Option option;
    public int actualQuestionNumber;
    public int numberOfQuestions;
}
