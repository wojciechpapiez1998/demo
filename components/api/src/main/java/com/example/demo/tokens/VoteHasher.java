package com.example.demo.tokens;

import com.example.demo.model.Answer;
import com.example.demo.model.Vote;
import com.google.common.hash.Hashing;

import java.nio.charset.StandardCharsets;
import java.util.List;

public class VoteHasher {

    public String hashVote(Vote vote, List<Answer> answers) {
        StringBuilder builder = new StringBuilder();
        builder.append(vote.id)
                .append(vote.date.getTime());
        if(answers != null){
            for (Answer answer : answers) {
                builder.append(answer.id)
                        .append(answer.questionid)
                        .append(answer.optionId);
            }

        }

        String stringToHash = builder.toString();
        String sha256hex = Hashing.sha256()
                .hashString(stringToHash, StandardCharsets.UTF_8)
                .toString();

        return sha256hex;
    }


}
