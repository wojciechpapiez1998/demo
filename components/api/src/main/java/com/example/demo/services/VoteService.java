package com.example.demo.services;

import com.example.demo.dto.QuestionInfo;
import com.example.demo.model.Answer;
import com.example.demo.model.Vote;
import com.example.demo.repository.VoteRepository;
import com.example.demo.tokens.VoteHasher;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.List;

@Service("VoteService")
public class VoteService implements VoteRepository {
    Logger logger = LoggerFactory.getLogger(AnswerService.class);

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private SurveyService surveyRepository;

    @Autowired
    private QuestionService questionRepository;

    @Autowired
    private AnswerService answersRepository;


    @Override
    public Vote addNewVote(long questionId) {
        Transaction transaction = null;
        Vote vote = null;
        try (Session session = sessionFactory.openSession()) {
            var question = questionRepository.findQuestionById(questionId);
            if (question != null) {
                var survey = surveyRepository.findSurveyById(question.surveyId);
                if (survey != null) {
                    vote = new Vote();
                    vote.date = new Date(System.currentTimeMillis());
                    vote.surveyId = survey.id;
                    transaction = session.beginTransaction();
                    // save before for generate id used to hash vote
                    session.save(vote);
                    session.save(vote);
                    transaction.commit();
                }
            }
            transaction = session.beginTransaction();
            session.save(vote);
            transaction.commit();
        } catch (Exception e) {
            logger.debug(e.getMessage(), e);
            if (transaction != null) {
                transaction.rollback();
            }
        }
        return vote;
    }

    @Override
    public Vote getLastAddedVote() {
        try (Session session = sessionFactory.openSession()) {
            String query = "FROM Vote where id = (select max(id) from Vote)";
            var vote = session.createQuery(query, Vote.class).uniqueResultOptional();
            return vote.orElse(null);
        } catch (Exception e) {
            logger.debug(e.getMessage(), e);
            return null;
        }
    }

    @Override
    public List<QuestionInfo> getSurveyByToken(String token) {
        try (Session session = sessionFactory.openSession()) {
            String query = "from Vote where hashed_identifier = '" + token.trim() + "'";
            var voteOptional = session.createQuery(query, Vote.class).uniqueResultOptional();
            if (voteOptional.isPresent()) {
                var vote = voteOptional.get();
                var surveyId = vote.surveyId;
                var survey = surveyRepository.findSurveyById(surveyId);
                if (survey != null) {
                    return questionRepository.getQuestionsFromSurveyByTitle(survey.title);
                }
            }
        } catch (Exception e) {
            logger.debug(e.getMessage(), e);
        }
        return null;
    }

    @Override
    public List<Answer> getAnswersByToken(String token) {
        try (Session session = sessionFactory.openSession()) {
            String query = "from Vote where hashed_identifier = '" + token.trim() + "'";
            var voteOptional = session.createQuery(query, Vote.class).uniqueResultOptional();
            if (voteOptional.isPresent()) {
                var vote = voteOptional.get();
                return  answersRepository.getAllUserAnswersInVote(vote.id);
            }
        } catch (Exception e) {
            logger.debug(e.getMessage(), e);
        }
        return null;
    }
}
