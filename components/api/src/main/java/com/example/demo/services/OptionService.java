package com.example.demo.services;

import com.example.demo.model.Option;
import com.example.demo.model.Question;
import com.example.demo.repository.OptionRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service("OptionService")
public class OptionService implements OptionRepository {
    Logger logger = LoggerFactory.getLogger(QuestionService.class);

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Option addNewOption(String name, long questionId) {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            Option option = new Option();
            option.name = name;
            option.questionId = questionId;
            transaction = session.beginTransaction();
            session.save(option);
            transaction.commit();
            return option;
        } catch (Exception e) {
            logger.debug(e.getMessage(), e);
            if (transaction != null) {
                transaction.rollback();
            }
        }
        return null;
    }

    @Override
    public Option findOptionById(long id) {
        try (Session session = sessionFactory.openSession()) {
            String query = "FROM Option WHERE id =" + id;
            var optionOptional = session.createQuery(query, Option.class).uniqueResultOptional();
            return optionOptional.orElse(null);
        } catch (Exception e) {
            logger.debug(e.getMessage(), e);
        }
        return null;
    }
}
