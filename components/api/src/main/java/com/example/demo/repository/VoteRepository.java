package com.example.demo.repository;

import com.example.demo.dto.QuestionInfo;
import com.example.demo.model.Answer;
import com.example.demo.model.Vote;

import java.util.List;

public interface VoteRepository {
    Vote addNewVote(long questionId);
    Vote getLastAddedVote();
    List<QuestionInfo> getSurveyByToken(String token);
    List<Answer> getAnswersByToken(String token);
}
