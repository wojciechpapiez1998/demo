package com.example.demo.model;

import javax.persistence.*;

@Entity
@Table(name = "role")
public class Role {

    @Id
    @GeneratedValue
    public Long id;
    @Column(unique=true)
    public String name;
}
