import http from "../http-common";

class UserDataService  {

    async signUp(data) {
        return  (await http.post("/user/signUp", data)).data;
    }

    async signIn(data) {
        return  (await http.post("/user/signIn", data)).data;
    }

    async signOut() {
        await http.get("/user/signOut");
    }

    async validateLogin(data) {
        return  (await http.post("/user/validateLogin", data)).data;
    }

    async validateEmailDomain(data) {
        return  (await http.post("/user/validateEmailDomain", data)).data;
    }

    async validateEmail(data) {
        return (await http.post("/user/validateEmail", data)).data;
    }

    async editMyAccount(id, data) {
        return (await http.put(`/user/${id}`, data)).data;
    }


    async getUserSession() {
        return (await http.get("/user/sessionInfo")).data;
    }

    async getRole(id) {
        return (await http.get(`/roles/${id}`)).data;
    }


    async getAdminRole() {
        return (await http.get("/roles/admin")).data;
    }

    async getUsers() {
        return (await http.get("/user/getAll")).data;
    }

    async deleteUser(id) {
        return (await http.delete(`/user/del/${id}`)).data;
    }

    async addAdmin(id) {
        return (await http.post(`/user/addAdmin/${id}`)).data;
    }

    async deleteAdmin(id) {
        return (await http.post(`/user/delAdmin/${id}`)).data;
    }

    async checkIfUserIsAdminOrOwner(id) {
        return (await http.get(`/user/check-if/admin/owner/${id}`)).data;
    }

    async checkIfUserIsOwner(id) {
        return (await http.get(`/user/check-if/owner/${id}`)).data;
    }

}

export default new UserDataService();