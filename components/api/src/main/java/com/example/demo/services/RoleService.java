package com.example.demo.services;

import com.example.demo.model.Role;
import com.example.demo.model.User;
import com.example.demo.repository.RoleRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service("RoleService")
public class RoleService implements RoleRepository {
    Logger logger = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Role createRole(String name) {
        var isRoleExist = checkIfRoleExists(name);
        if (isRoleExist) {
            return null;
        }
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            Role role = new Role();
            role.name = name;

            transaction = session.beginTransaction();
            session.save(role);
            transaction.commit();
            return role;
        } catch (Exception e) {
            logger.debug(e.getMessage(), e);
            if (transaction != null) {
                transaction.rollback();
            }
        }
        return null;
    }

    @Override
    public Role findRoleByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            String query = "from Role where name = '" + name.trim() + "'";
            var role = session.createQuery(query, Role.class).uniqueResultOptional();
            return role.orElse(null);
        }
        catch (Exception e) {
            logger.debug(e.getMessage(), e);
            return null;
        }

    }

    @Override
    public Role findRoleById(long id) {
        try (Session session = sessionFactory.openSession()) {
            String query = "FROM Role WHERE id =" + id;
            var role = session.createQuery(query, Role.class).uniqueResultOptional();
            return role.orElse(null);
        }
        catch (Exception e) {
            logger.debug(e.getMessage(), e);
            return null;
        }
    }


    @Override
    public Boolean checkIfRoleExists(String name) {
        try (Session session = sessionFactory.openSession()) {
            String query = "from Role where name = '" + name.trim() + "'";
            var result = session.createQuery(query, Role.class).uniqueResultOptional();
            return result.isPresent();
        }
        catch (Exception e) {
            logger.debug(e.getMessage(), e);
            return null;
        }
    }

    @Override
    public boolean checkIfUserIsAdminOrOwner(long userId) {
        try (Session session = sessionFactory.openSession()) {
            String query = "FROM User WHERE id =" + userId;
            var userOptional = session.createQuery(query, User.class).uniqueResultOptional();
            if(userOptional.isPresent()){
                var user = userOptional.get();
                var admin = findRoleByName("Admin");
                var owner = findRoleByName("Owner");
                if(user.roleId.equals(admin.id) || user.roleId.equals(owner.id)){
                    return true;
                }
            }
        }
        catch (Exception e) {
            logger.debug(e.getMessage(), e);
        }
        return false;
    }

    @Override
    public boolean checkIfUserIsOwner(long userId) {
        try (Session session = sessionFactory.openSession()) {
            String query = "FROM User WHERE id =" + userId;
            var userOptional = session.createQuery(query, User.class).uniqueResultOptional();
            if(userOptional.isPresent()){
                var user = userOptional.get();
                var owner = findRoleByName("Owner");
                if(user.roleId.equals(owner.id)){
                    return true;
                }
            }
        }
        catch (Exception e) {
            logger.debug(e.getMessage(), e);
        }
        return false;
    }
}
