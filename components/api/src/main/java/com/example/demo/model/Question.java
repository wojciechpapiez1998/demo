package com.example.demo.model;


import javax.persistence.*;

@Entity
@Table(name = "question")
public class Question{

    @Id
    @GeneratedValue
    public Long id;
    public String name;
    public boolean multipleVotes;
    public Long surveyId;
}
