package com.example.demo.model;


import javax.persistence.*;

@Entity
@Table(name = "survey")
public class Survey{

    @Id
    @GeneratedValue
    public Long id;
    @Column(unique=true)
    public String title;
    public Boolean deleted = false;

}
