import PollView from './PollAnswer.vue';
import PollCreator from './PollCreator.vue'

module.exports = {
  install: function (Vue) {
    Vue.component('poll-view', PollView);
    Vue.component('poll-creator', PollCreator);
  }
};